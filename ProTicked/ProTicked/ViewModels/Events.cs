﻿using System;
namespace ProTicked.ViewModels
{
    public static class Events
    {
        public static string ItemAdded = "AddItem";
        public static string ItemUpdated = "UpdateItem";
    }
}