﻿using ProTicked.Models;
using ProTicked.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProTicked.ViewModels
{
    public class ItemsDetailViewModel
    {
        private readonly ItemDataStore _itemStore;
        private readonly IPageService _pageService;
        public Item Item { get; private set; }
        public ICommand SaveCommand { get; private set; }
        public ItemsDetailViewModel(ItemViewModel viewModel, ItemDataStore itemStore, IPageService pageService)
        {
            if (viewModel == null)
                throw new ArgumentNullException(nameof(viewModel));

            _pageService = pageService;
            _itemStore = itemStore;
            SaveCommand = new Command(async () => await Save());
            Item = new Item
            {
                Id = viewModel.Id,
                Text = viewModel.Text,
                Description = viewModel.Description,
            };
        }

        async Task Save()
        {
            if (String.IsNullOrWhiteSpace(Item.Text) && String.IsNullOrWhiteSpace(Item.Description))
            {
                await _pageService.DisplayAlert("Error", "Please enter the text and description.", "OK");
                return;
            }
            if (Item.Id == 0)
            {
                await _itemStore.AddItem(Item);
                MessagingCenter.Send(this, Events.ItemAdded, Item);
            }
            else
            {
                await _itemStore.UpdateItem(Item);
                MessagingCenter.Send(this, Events.ItemUpdated, Item);
            }
            await _pageService.PopAsync();
        }
    }
}
