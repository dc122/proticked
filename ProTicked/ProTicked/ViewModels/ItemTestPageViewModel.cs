﻿using ProTicked.Models;
using ProTicked.Services;
using ProTicked.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProTicked.ViewModels
{
    public class ItemsTestPageViewModel : BaseViewModel
    {
        private ItemViewModel _selectedItem;
        private ItemDataStore _itemStore;
        private IPageService _pageService;

        private bool _isDataLoaded;

        public ObservableCollection<ItemViewModel> Items { get; private set; }
            = new ObservableCollection<ItemViewModel>();

        public ItemViewModel SelectedItem
        {
            get { return _selectedItem; }
            set { SetValue(ref _selectedItem, value); }
        }

        public ICommand LoadDataCommand { get; private set; }
        public ICommand AddItemCommand { get; private set; }
        public ICommand SelectItemCommand { get; private set; }
        public ICommand DeleteItemCommand { get; private set; }
        public ICommand CallItemCommand { get; private set; }

        public ItemsTestPageViewModel(ItemDataStore itemStore, IPageService pageService)
        {
            _itemStore = itemStore;
            _pageService = pageService;

            LoadDataCommand = new Command(async () => await LoadData());
            AddItemCommand = new Command(async () => await AddItem());
            SelectItemCommand = new Command<ItemViewModel>(async i => await SelectItem(i));
            DeleteItemCommand = new Command<ItemViewModel>(async i => await DeleteItem(i));
            CallItemCommand = new Command<ItemViewModel>(async i => await CallItem(i));

            MessagingCenter.Subscribe<ItemsDetailViewModel, Item>
                (this, Events.ItemAdded, OnItemAdded);

            MessagingCenter.Subscribe<ItemsDetailViewModel, Item>
            (this, Events.ItemUpdated, OnItemUpdated);
        }

        private async Task LoadData()
        {
            if (_isDataLoaded)
                return;

            _isDataLoaded = true;
            var items = await _itemStore.GetItemsAsync();
            foreach (var item in items)
                Items.Add(new ItemViewModel(item));
        }

        private async Task AddItem()
        {
            await _pageService.PushAsync(new ItemsDetailPage(new ItemViewModel()));
        }

        private async Task SelectItem(ItemViewModel item)
        {
            if (item == null)
                return;

            SelectedItem = null;
            await _pageService.PushAsync(new ItemsDetailPage(item));
        }

        private async Task DeleteItem(ItemViewModel itemViewModel)
        {
            if (await _pageService.DisplayAlert("Warning", $"Are you sure you want to delete {itemViewModel.Text}?", "Yes", "No"))
            {
                Items.Remove(itemViewModel);

                var item = await _itemStore.GetItem(itemViewModel.Id);
                await _itemStore.DeleteItem(item);
            }
        }

        private async Task CallItem(ItemViewModel itemViewModel)
        {
            if (itemViewModel != null)
            {
                var message = itemViewModel.Text + "\n" + itemViewModel.Description;
                bool isCall = await _pageService.DisplayAlert("Do you really want to Call?", message, "Call", "Cancel");
                if (isCall)
                {
                    //Go to Contacts App
                    Device.OpenUri(new Uri("tel:" + itemViewModel.Id));
                }
            }
        }

        public ItemsTestPageViewModel()
        {
            MessagingCenter.Subscribe<ItemsDetailViewModel, Item>
                (this, Events.ItemAdded, OnItemAdded);

            MessagingCenter.Subscribe<ItemsDetailViewModel, Item>
            (this, Events.ItemUpdated, OnItemUpdated);
        }

        private void OnItemAdded(ItemsDetailViewModel source, Item item)
        {
            Items.Add(new ItemViewModel(item));
        }

        private void OnItemUpdated(ItemsDetailViewModel source, Item item)
        {
            var itemInList = Items.Single(c => c.Id == item.Id);

            itemInList.Id = item.Id;
            itemInList.Text = item.Text;
            itemInList.Description = item.Description;
        }
    }
}
