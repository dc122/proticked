﻿using ProTicked.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProTicked.ViewModels
{
    public class ItemViewModel : BaseViewModel
    {
        public int Id { get; set; }

        public ItemViewModel() { }

        public ItemViewModel(Item item)
        {
            Id = item.Id;
            Text = item.Text;
            Description = item.Description;
        }

        private string _text;
        public string Text
        {
            get { return _text; }
            set
            {
                SetValue(ref _text, value);
            }
        }

        private string _description;
        public string Description
        {
            get { return _description; }
            set
            {
                SetValue(ref _description, value);
            }
        }
    }
}
