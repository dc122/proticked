﻿using ProTicked.Persistence;
using ProTicked.Services;
using ProTicked.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProTicked.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsDetailPage : ContentPage
    {
        public ItemsDetailPage(ItemViewModel viewModel)
        {
            InitializeComponent();
            var itemStore = new ItemDataStore(DependencyService.Get<ISQLiteDb>());
            var pageService = new PageService();
            Title = (viewModel.Id == 0) ? "New Item" : "Edit Item";
            BindingContext = new ItemsDetailViewModel(viewModel ?? new ItemViewModel(), itemStore, pageService);
        }
    }
}