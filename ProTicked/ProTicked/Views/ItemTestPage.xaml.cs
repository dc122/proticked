﻿using ProTicked.Persistence;
using ProTicked.Services;
using ProTicked.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProTicked.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemTestPage : ContentPage
    {
        public ItemTestPage()
        {
            var contactStore = new ItemDataStore(DependencyService.Get<ISQLiteDb>());
            var pageService = new PageService();

            ViewModel = new ItemsTestPageViewModel(contactStore, pageService);

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            ViewModel.LoadDataCommand.Execute(null);
            base.OnAppearing();
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ViewModel.SelectItemCommand.Execute(e.SelectedItem);
        }

        public ItemsTestPageViewModel ViewModel
        {
            get { return BindingContext as ItemsTestPageViewModel; }
            set { BindingContext = value; }
        }
    }
}