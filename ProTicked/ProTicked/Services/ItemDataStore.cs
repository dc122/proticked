﻿using ProTicked.Models;
using ProTicked.Persistence;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProTicked.Services
{
    public class ItemDataStore : IDataStore<Item>
    {
        private SQLiteAsyncConnection _connection;
        public ItemDataStore(ISQLiteDb db)
        {
            _connection = db.GetConnection();
            _connection.CreateTableAsync<Item>();
        }

        public async Task AddItem(Item item)
        {
            await _connection.InsertAsync(item);
        }

        public async Task UpdateItem(Item item)
        {
            await _connection.UpdateAsync(item);
        }

        public async Task DeleteItem(Item item)
        {
            await _connection.DeleteAsync(item);
        }

        public async Task<Item> GetItem(int id)
        {
            return await _connection.FindAsync<Item>(id);
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await _connection.Table<Item>().ToListAsync();
        }
    }
}
