﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProTicked.Services
{
    public interface IDataStore<T>
    {
        Task AddItem(T item);
        Task UpdateItem(T item);
        Task DeleteItem(T item);
        Task<T> GetItem(int id);
        Task<IEnumerable<T>> GetItemsAsync(bool forceRefresh = false);
    }
}
